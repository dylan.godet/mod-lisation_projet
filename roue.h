#ifndef ROUE_H
#define ROUE_H
#include <string>

class Roue
{
//Constructeur/Destructeur
public:

  Roue();
  Roue(int,double);
  ~Roue(); 


  //Attributs

private:
  int poid;
  double diametre;
      


  //Méthodes
public:
  void rouler();
};
#endif // ROUE_H
