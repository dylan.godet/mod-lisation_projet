#ifndef VITRE_H
#define VITRE_H
#include <string>

class Vitre
{

private:
  
//attributs
  int pourcentage; //pourcentage d'ouverture de la vitre
  std::string position; // la position de la vitre (gauche droite)
  
  //methodes

public:

  Vitre();
  Vitre(std::string);
  Vitre(int, std::string);
  ~Vitre();

  void ouverture(int);

  int getPourcentage();
  

  //methode qui permet d'ouvrir une vitre par rapport à sa position(vitre gauche, droite) à un pourcentage defini en paramètre 
    
  
};
#endif//VITRE_H
