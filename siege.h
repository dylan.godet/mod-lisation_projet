#ifndef SIEGE_H
#define SIEGE_H
class Siege
{
//Attributs

private:
    int hauteur;
    int longueur; // Avancement ou reculement du siège
    int inclinaison;
    double temperature;

//Construcreur/Destructeur par défaut

public:
    Siege();
    ~Siege();

//Constructeur avec paramètres
    Siege(int, int ,int, double);

//Méthodes

void chauffer(double);
void positionnerhauteur(int);
void positionnerlongueur(int);
void positionnerinclinaison(int);


};
#endif //SIEGE_H
