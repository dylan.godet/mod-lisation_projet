#ifndef PORTIERE_H
#define PORTIERE_H
#include "vitre.h"

#include <string>

class Portiere
{


//Constructeur/Destructeur
public:

  Portiere();
  Portiere(bool, std::string, std::string, int, Vitre);
  ~Portiere();



//Attributs
private:

  bool verrouillee;
  std::string position; //avant arrière
  std::string lateralisation; //droite gauche
  int angle;
  Vitre vitre;
  
//un attribut ma_vitre de type vitre (pas encore crée)

//Méthodes
  
public:

 void afficher();    

  void ouvrir(int);
  
  void fermer();
  
  void ouvrir_vitre(int);
  
  void verrouiller(bool);
  
  void deverrouiller();

  bool est_verrouillee();
  bool est_ouvert();


};

#endif //PORTIERE_H
