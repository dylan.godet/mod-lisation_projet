#include<iostream>
#include "vitre.h"
using namespace std;

//COnstructeur par defaut
Vitre::Vitre() :
  pourcentage(0),
  position("")
{
  cout << "Constructeur par défaut de la classe Vitre" << endl;
}

//COnstructeurs avec paramètres
Vitre::Vitre(std::string _position){
  pourcentage = 0;
  position= _position;
  cout << "Constructeur avec paramètre de la classe Vitre" << endl;
}

Vitre::Vitre(int _pourcentage, std::string _position)
{
  pourcentage = _pourcentage;
  position = _position;
  cout << "Constructeur avec paramètres de la classe Vitre" << endl;
}

//deconstructeur
Vitre::~Vitre(){};



//Implémentation des méthodes
void Vitre::ouverture(int _pourcentage)
{
  cout<<"Vous avez ouvert votre vitre "<<position<<" à : "<<_pourcentage<<" %"<<endl;
  
}

int Vitre::getPourcentage() {
  return pourcentage;
}
