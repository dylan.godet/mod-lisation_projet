#include "siege.h"
#include <iostream>

using namespace std;



//Constructeur par défaut

Siege::Siege():
  hauteur(0),
  longueur (0),
  inclinaison (0),
  temperature (0)

{
  cout<<"Création d'un siège";
}



//Constructeur avec paramètre

Siege::Siege(int _hauteur,
             int _longueur, //positionnement longueur
             int _inclinaison,
             double _temperature):

  hauteur (_hauteur),
    longueur (_longueur),
  inclinaison (_inclinaison),
    temperature (_temperature)
 {
   
 }
                 
//Destructeur

Siege::~Siege(){};

// Méthodes 

void Siege::positionnerhauteur(int _hauteur)
{
 cout << _hauteur<<" cm  ";
};


void Siege::positionnerlongueur(int _longueur){};
void Siege::positionnerinclinaison(int _inclinaison){};
void Siege::chauffer(double _temperature){};
