#include<iostream>
#include "portiere.h"
using namespace std;


//Constructeur par défaut
Portiere::Portiere() :
  verrouillee(true),
  position(""),
  lateralisation(""),
  angle(0),
  vitre()
{
  cout << "Constructeur par défaut de la classe Portiere" << endl;
}

//Constructeur avec paramètres
Portiere::Portiere (bool _verrouillee,
                    std::string _position,
                    std::string _lateralisation,
                    int _angle,
                    Vitre _v) :
  verrouillee(_verrouillee),
  position(_position),
  lateralisation(_lateralisation),
  angle(_angle),
  vitre(_v)


  
{
  cout << "Constructeur de Portiere avec paramètres dont une Vitre dont le pourcentage est " << vitre.getPourcentage() << endl;
}
  
//deconstructeur  
Portiere::~Portiere(){};


//implémentation des méthodes


void Portiere::afficher() 
{
cout<< "angle : " << angle ;
}


void Portiere::ouvrir(int _angle=90){

  if(!verrouillee){
    angle= _angle;
    }
}

bool Portiere::est_ouvert() {

}



bool Portiere::est_verrouillee(){

}



void Portiere::fermer(){

  if(angle>0) {
    angle=0;
  }

}

void Portiere::ouvrir_vitre(int){

}







void Portiere::verrouiller(bool){

}

;
